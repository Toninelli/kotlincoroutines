package com.example.kotlincoroutines.extension

import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowViaChannel
import kotlin.coroutines.suspendCoroutine


fun <T> List<T>.nullIfEmpty(): List<T>? {
    return if(this.isEmpty())
        null
    else
        this
}
