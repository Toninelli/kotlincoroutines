package com.example.kotlincoroutines.extension

import androidx.activity.viewModels
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.*
import com.example.kotlincoroutines.util.ViewModelFactory

inline fun <reified T : ViewModel> Fragment.viewModel(factory: ViewModelFactory,body: T.() -> Unit = {}): T {

    val viewModel by viewModels<T>{factory}
    viewModel.body()
    return viewModel
}

inline fun <reified T : ViewModel> FragmentActivity.viewModel(factory: ViewModelFactory, body: T.() -> Unit = {}): T {

    val viewModel by viewModels<T>{factory}
    viewModel.body()
    return viewModel
}



fun <T: Any, L: LiveData<T>> LifecycleOwner.observe(liveData: L, f:(T?) -> Unit){
    liveData.observe(this, Observer {
        f(it)
    })
}
