package com.example.kotlincoroutines.domain

import com.example.kotlincoroutines.data.model.Post
import com.example.kotlincoroutines.data.repo.PostRepo
import com.example.kotlincoroutines.vo.Either
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class PostDbUseCase @Inject constructor(val repo: PostRepo): FlowUseCase<Any, List<Post>>(){
    override suspend fun exec(param: Any?): Flow<List<Post>> {
        return repo.getPostFromDb()
    }

}