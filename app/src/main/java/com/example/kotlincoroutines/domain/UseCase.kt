package com.example.kotlincoroutines.domain

import androidx.lifecycle.MutableLiveData
import com.example.kotlincoroutines.data.model.Post
import com.example.kotlincoroutines.data.repo.PostRepo
import com.example.kotlincoroutines.vo.Either
import com.example.kotlincoroutines.vo.Listing
import com.example.kotlincoroutines.vo.Resource
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

abstract class UseCase<P, R> : AbstractUseCase<P, Either<Exception, R>>() {

    var resultLiveData = MutableLiveData<Resource<R>>()

    abstract override suspend fun exec(param: P?): Either<Exception, R>

    override fun invoke(param: P?, onResult: (Either<Exception, R>) -> Unit) {
        CoroutineScope(Dispatchers.IO).launch {
            resultLiveData.postValue(Resource.Loading(null))
            exec(param).also {
                onResult(it)
                it.either(
                    {
                        resultLiveData.postValue(Resource.Error(it))
                    }, {
                        resultLiveData.postValue(Resource.Success(it))
                    })
            }

        }
    }
}

abstract class PagedUseCase<P, R> : AbstractUseCase<P, Listing<R>>() {

    var resultLiveData = MutableLiveData<Listing<R>>()

    abstract override suspend fun exec(param: P?): Listing<R>

    override fun invoke(param: P?, onResult: (Listing<R>) -> Unit) {
        CoroutineScope(Dispatchers.IO).launch {
            exec(param).also {
                resultLiveData.postValue(it)
                onResult(it)
            }

        }
    }

}


@ExperimentalCoroutinesApi
abstract class FlowUseCase<P,R>: AbstractUseCase<P, Flow<R>>(){

    var resultLiveData = MutableLiveData<Resource<R>>()

    abstract override suspend fun exec(param: P?): Flow<R>

    override fun invoke(param: P?, onResult: (Flow<R>) -> Unit) {
        CoroutineScope(Dispatchers.IO).launch {
            exec(param).also { onResult(it) }
                .onStart { resultLiveData.postValue(Resource.Loading()) }
                .collect { resultLiveData.postValue(Resource.Success(it)) }

        }
    }

}

abstract class AbstractUseCase<P, R> {

    protected abstract suspend fun exec(param: P?): R

    abstract operator fun invoke(param: P?, onResult: ((R) -> Unit) = {})
}
