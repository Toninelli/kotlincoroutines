package com.example.kotlincoroutines.domain

import com.example.kotlincoroutines.data.model.Post
import com.example.kotlincoroutines.data.repo.PostRepo
import com.example.kotlincoroutines.vo.Either
import javax.inject.Inject

class PostUseCase @Inject constructor(private val repo: PostRepo) : UseCase<Any, List<Post>>() {

    override suspend fun exec(param: Any?): Either<Exception, List<Post>> {
        return repo.getPosts()
    }


}