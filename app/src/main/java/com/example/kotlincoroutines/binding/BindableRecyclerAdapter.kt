package com.example.kotlincoroutines.binding

import androidx.paging.PagedList
import com.example.kotlincoroutines.vo.Resource

interface BindableListAdapter<T> {
    fun setData(data: Resource<T>)
}

interface BindablePagedListAdapter<T> {
    fun setData(resource: PagedList<T>)
}
