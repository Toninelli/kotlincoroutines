package com.example.kotlincoroutines.binding

interface ResourceListener{
    fun onResourceSuccess(){}
    fun onResourceError(exception: Exception){}
    fun onResourceLoading(){}
}