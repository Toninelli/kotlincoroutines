package com.example.kotlincoroutines.binding

import android.view.View
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView

import com.example.kotlincoroutines.vo.Resource
import com.example.kotlincoroutines.vo.case

@BindingAdapter("showHide")
fun showHide(view: View, b: Boolean) {
    view.visibility = if (b) View.VISIBLE else View.GONE
}

@Suppress("UNCHECKED_CAST")
@BindingAdapter("adapterResourceData")
fun <T> bindAdapter(view: RecyclerView, resource: Resource<T>?) {

    resource?.let {
        if (view.adapter is BindableListAdapter<*>) {
            (view.adapter as BindableListAdapter<T>).setData(it)
        }
    }
}

@BindingAdapter("uiListener", "resource")
fun <T> onResourceListener(view: View, listener: ResourceListener, resource: Resource<T>?){
    resource?.case(success = {listener.onResourceSuccess()}, error = {listener.onResourceError(it.exception)})
}