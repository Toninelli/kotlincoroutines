package com.example.kotlincoroutines

import android.app.Activity
import android.app.Application
import com.example.kotlincoroutines.di.AppInjector
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import javax.inject.Inject

class MyApplication: Application(), HasAndroidInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun onCreate() {
        super.onCreate()

//        startKoin {
//            androidContext(this@MyApplication)
//            modules(appModule)
//        }

        AppInjector.init(this)

    }

    override fun androidInjector(): AndroidInjector<Any> = dispatchingAndroidInjector


}