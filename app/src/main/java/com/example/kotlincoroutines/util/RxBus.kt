package com.example.kotlincoroutines.util

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.ReplaySubject

object RxBus {

    private val compositeMap = HashMap<String, CompositeDisposable>()
    private val subjectMap = HashMap<RxEvent, PublishSubject<Any>>()
    private val subjectMapReplay = mutableMapOf<RxEventType, ReplaySubject<RxEvent>?>()
    private val subjectMapPublic = mutableMapOf<RxEventType, PublishSubject<RxEvent>?>()

    /**
     * publish on PublishSubject
     */
    fun publishPublic(eventValue: RxEvent) {
        val subj = getSubjectPublic(eventValue.getRxEventType())
        subj.onNext(eventValue)
    }

    /**
     * publish on ReplaySubject
     */
    fun publishReplay(eventValue: RxEvent) {
        val subj = getSubjectReplay(eventValue.getRxEventType())
        subj.onNext(eventValue)
    }

    /**
     * listen PublishSubject
     */
    fun <T : RxEvent> listenPublic(eventType: RxEventType, string: String, f: (T) -> Unit) {

        getComposite(string).add(
            getSubjectPublic(eventType)
                .doOnDispose {
                    subjectMapPublic[eventType] = null
                }.subscribe {
                    val value = it as T
                    f(value)
                })

    }

    /**
     * listen ReplaySubject
     */
    fun <T : RxEvent> listenReplay(eventType: RxEventType, string: String, f: (T) -> Unit) {
        
        getComposite(string).add(
            getSubjectReplay(eventType)
                .doOnDispose {
                    subjectMapReplay[eventType] = null
                }.subscribe {
                    val value = it as T
                    f(value)
                })

    }

    private fun getSubjectReplay(eventEventType: RxEventType): ReplaySubject<RxEvent> {
        var subject = subjectMapReplay[eventEventType]
        if (subject == null) {
            subject = ReplaySubject.create()
            subjectMapReplay[eventEventType] = subject
        }

        return subject
    }

    private fun getSubjectPublic(eventEventType: RxEventType): PublishSubject<RxEvent> {
        var subject = subjectMapPublic[eventEventType]
        if (subject == null) {
            subject = PublishSubject.create()
            subjectMapPublic[eventEventType] = subject
        }

        return subject
    }

    private fun getComposite(string: String): CompositeDisposable {
        var compositeDisposable = compositeMap[string]
        if (compositeDisposable == null) {
            compositeDisposable = CompositeDisposable()
            compositeMap[string] = compositeDisposable
        }
        return compositeDisposable
    }


    fun clear(string: String) {
        getComposite(string).clear()
    }

}

enum class RxEventType {
    STRING,
    ERROR
}

sealed class RxEvent(private val type: RxEventType) {

    data class EventString(val value: String) : RxEvent(RxEventType.STRING)
    data class ErrorType(val error: String): RxEvent(RxEventType.ERROR)

    fun getRxEventType(): RxEventType {
        return this.type
    }

}



