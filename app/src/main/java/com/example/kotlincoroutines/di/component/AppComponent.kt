package com.example.kotlincoroutines.di.component

import com.example.kotlincoroutines.MyApplication
import com.example.kotlincoroutines.di.module.AppModule
import com.example.kotlincoroutines.di.scope.ApplicationScope
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule

@ApplicationScope
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        SubComponentBuilder::class
    ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(app: MyApplication): Builder

        fun build(): AppComponent
    }

    fun inject(app: MyApplication)
}