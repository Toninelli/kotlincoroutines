package com.example.kotlincoroutines.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
