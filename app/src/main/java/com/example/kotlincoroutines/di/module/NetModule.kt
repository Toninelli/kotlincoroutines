package com.example.kotlincoroutines.di.module

import com.example.kotlincoroutines.data.api.RemoteApi
import com.example.kotlincoroutines.di.scope.ApplicationScope
import com.example.networkcalladapterlib.NetworkCallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named

@Module
class NetModule{

    @ApplicationScope
    @Provides
    fun provideApiService(): RemoteApi {
        return Retrofit.Builder()
            .baseUrl("https://jsonplaceholder.typicode.com/")
            .addCallAdapterFactory(NetworkCallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(RemoteApi::class.java)
    }

}