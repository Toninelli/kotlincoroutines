package com.example.kotlincoroutines.di.scope

import javax.inject.Qualifier

@Qualifier
annotation class AppContext {
}