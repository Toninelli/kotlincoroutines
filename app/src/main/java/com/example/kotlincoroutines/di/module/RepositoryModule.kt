package com.example.kotlincoroutines.di.module

import com.example.kotlincoroutines.data.repo.PostRepo
import com.example.kotlincoroutines.data.repo.PostRepoImpl
import dagger.Binds
import dagger.Module

@Module
abstract class RepositoryModule{

    @Binds
    abstract fun getPostRepo(repo: PostRepoImpl): PostRepo

}