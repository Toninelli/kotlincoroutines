package com.example.kotlincoroutines.di.module

import com.example.kotlincoroutines.di.scope.FragmentScope
import com.example.kotlincoroutines.ui.PostFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityModule{


    @FragmentScope
    @ContributesAndroidInjector
    abstract fun getPostFragment():PostFragment

}