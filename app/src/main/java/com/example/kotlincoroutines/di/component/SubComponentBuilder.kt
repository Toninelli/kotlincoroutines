package com.example.kotlincoroutines.di.component

import com.example.kotlincoroutines.di.module.MainActivityModule
import com.example.kotlincoroutines.di.scope.ActivityScope
import com.example.kotlincoroutines.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class SubComponentBuilder {

    @ActivityScope
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun getMainActivity(): MainActivity
}
