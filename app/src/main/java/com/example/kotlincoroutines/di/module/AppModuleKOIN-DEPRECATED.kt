package com.example.kotlincoroutines.di.module

import com.example.kotlincoroutines.data.api.RemoteApi

import com.example.kotlincoroutines.data.repo.PostRepo
import com.example.kotlincoroutines.data.repo.PostRepoImpl
import com.example.kotlincoroutines.domain.PostUseCase
import com.example.kotlincoroutines.ui.PostViewModel
import com.example.networkcalladapterlib.NetworkCallAdapterFactory
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


//val appModule = module {
//
//    factory {
//        Retrofit.Builder()
//            .baseUrl("https://jsonplaceholder.typicode.com/")
//            .addCallAdapterFactory(NetworkCallAdapterFactory.create())
//            .addConverterFactory(GsonConverterFactory.create())
//            .build()
//            .create(RemoteApi::class.java)
//    }
//
//
//    single<PostRepo>{ PostRepoImpl(get()) }
//
//    single { PostUseCase(get()) }
//
//    viewModel { PostViewModel(get()) }
//
//
//
//}
