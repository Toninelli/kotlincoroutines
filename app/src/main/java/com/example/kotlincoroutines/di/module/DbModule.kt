package com.example.kotlincoroutines.di.module

import android.content.Context
import androidx.room.Room
import com.example.kotlincoroutines.data.db.AppDatabase
import com.example.kotlincoroutines.data.db.PostDao
import com.example.kotlincoroutines.di.scope.AppContext
import com.example.kotlincoroutines.di.scope.ApplicationScope
import dagger.Module
import dagger.Provides

@Module
class DbModule {

    @ApplicationScope
    @Provides
    fun provideDb(context: Context):AppDatabase{
        return Room.databaseBuilder(context,AppDatabase::class.java,"KCDB").fallbackToDestructiveMigration().build()
    }

    @ApplicationScope
    @Provides
    fun providePostDao(db: AppDatabase): PostDao{
        return db.getPostDao()
    }

}