package com.example.kotlincoroutines.di.module

import android.content.Context
import com.example.kotlincoroutines.MyApplication
import com.example.kotlincoroutines.di.scope.ApplicationScope
import dagger.Binds
import dagger.Module

@Module(includes = [NetModule::class, ViewModelModule::class, RepositoryModule::class, DbModule::class])
abstract class AppModule{

    @ApplicationScope
    @Binds
    abstract fun getContext(context: MyApplication): Context

}