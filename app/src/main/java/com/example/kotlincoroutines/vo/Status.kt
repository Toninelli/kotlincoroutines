package com.example.kotlincoroutines.vo

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}