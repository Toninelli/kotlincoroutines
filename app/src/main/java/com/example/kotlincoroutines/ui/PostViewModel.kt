package com.example.kotlincoroutines.ui

import android.view.View
import android.widget.Button
import androidx.lifecycle.*
import com.example.kotlincoroutines.data.model.Post
import com.example.kotlincoroutines.domain.PostDbUseCase
import com.example.kotlincoroutines.domain.PostUseCase
import com.example.kotlincoroutines.vo.Resource
import com.example.kotlincoroutines.vo.case
import javax.inject.Inject

class PostViewModel @Inject constructor(val useCase: PostUseCase) : ViewModel() {


    var result = useCase.resultLiveData

    fun exec(view: View) {
        useCase(null)

    }

}