package com.example.kotlincoroutines.ui


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.*
import androidx.navigation.fragment.findNavController
import androidx.room.Transaction

import com.example.kotlincoroutines.R
import com.example.kotlincoroutines.binding.ResourceListener
import com.example.kotlincoroutines.data.db.PostDao
import com.example.kotlincoroutines.data.model.Post
import com.example.kotlincoroutines.databinding.FragmentPostBinding
import com.example.kotlincoroutines.di.Injectable
import com.example.kotlincoroutines.extension.nullIfEmpty
import com.example.kotlincoroutines.extension.viewModel
import com.example.kotlincoroutines.util.ViewModelFactory
import com.example.kotlincoroutines.util.autoclearedValue
import kotlinx.coroutines.*
import javax.inject.Inject


class PostFragment : Fragment(), Injectable, ResourceListener {

    @Inject
    lateinit var factory: ViewModelFactory

    @Inject
    lateinit var dao: PostDao

    private lateinit var binding: FragmentPostBinding

    private lateinit var postViewModel: PostViewModel

    private var adapter by autoclearedValue<PostAdapter>()

    @ExperimentalCoroutinesApi
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_post, container, false)

        postViewModel = viewModel(factory)

        adapter = PostAdapter {
            findNavController().navigate(PostFragmentDirections.actionPostFragmentToUserFragment())
        }

        binding.listener = this

        binding.model = postViewModel
        binding.beerList.adapter = adapter
        binding.lifecycleOwner = viewLifecycleOwner


        return binding.root
    }

    override fun onResourceError(exception: Exception) {
        println("errore")
    }

}

interface Listener{
    fun onCallback(value: Int)
    fun cancel()
}
