package com.example.kotlincoroutines.ui

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

class PostViewHolder<out T: ViewDataBinding> constructor(val binding: T): RecyclerView.ViewHolder(binding.root)