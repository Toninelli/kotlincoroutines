package com.example.kotlincoroutines.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil

import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter

import com.example.kotlincoroutines.R
import com.example.kotlincoroutines.binding.BindableListAdapter
import com.example.kotlincoroutines.data.model.Post
import com.example.kotlincoroutines.databinding.PostItemBinding
import com.example.kotlincoroutines.vo.Resource
import com.example.kotlincoroutines.vo.case


class PostAdapter(
    private val callback: (Post) -> Unit
) : ListAdapter<Post, PostViewHolder<PostItemBinding>>(DIFF_CALLBACK), BindableListAdapter<List<Post>> {

    override fun setData(data: Resource<List<Post>>) {
        data.case (success = {submitList(it.data)}, loading = {submitList(null)})
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PostViewHolder<PostItemBinding> {

        val binding = DataBindingUtil.inflate<PostItemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.post_item,
            parent,
            false
        )

        binding.root.setOnClickListener {
            binding.post?.let {
                callback.invoke(it)
            }
        }

        return PostViewHolder(binding)

    }

    override fun onBindViewHolder(holder: PostViewHolder<PostItemBinding>, position: Int) {
        holder.binding.post = getItem(position)
    }


    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Post>() {
            override fun areItemsTheSame(oldItem: Post, newItem: Post): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Post, newItem: Post): Boolean {
                return oldItem.title == newItem.title
            }

        }

    }
}
