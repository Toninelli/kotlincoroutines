package com.example.kotlincoroutines.data.repo

import androidx.room.Transaction
import com.example.kotlincoroutines.data.api.*
import com.example.kotlincoroutines.data.db.PostDao

import com.example.kotlincoroutines.data.model.Post
import com.example.kotlincoroutines.vo.Either
import com.example.kotlincoroutines.vo.left
import com.example.kotlincoroutines.vo.right
import com.example.networkcalladapterlib.ResponseNetworkEmpty
import com.example.networkcalladapterlib.ResponseNetworkError
import com.example.networkcalladapterlib.ResponseNetworkSuccess
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject


interface PostRepo {

    suspend fun getPosts(): Either<Exception, List<Post>>

    suspend fun getPostFromDb(): Flow<List<Post>>

}

class PostRepoImpl @Inject constructor(private val api: RemoteApi, private val dao: PostDao) : PostRepo {


    override suspend fun getPostFromDb(): Flow<List<Post>> {

        return dao.getPost()
    }

    override suspend fun getPosts(): Either<Exception, List<Post>> {

        return api.getPost().run {
            when (this) {
                is ResponseNetworkError -> left(this.exception)

                is ResponseNetworkSuccess -> right(this.body)

                is ResponseNetworkEmpty -> left(Exception(this.message))

            }
        }

    }

}

