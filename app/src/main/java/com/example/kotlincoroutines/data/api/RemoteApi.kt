package com.example.kotlincoroutines.data.api

import com.example.kotlincoroutines.data.model.Post
import com.example.networkcalladapterlib.ResponseNetwork
import retrofit2.http.GET

interface RemoteApi {


    @GET("posts")
    suspend fun getPost(): ResponseNetwork<List<Post>>


}