package com.example.kotlincoroutines.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.kotlincoroutines.data.model.Post
import kotlinx.coroutines.flow.Flow

@Dao
interface PostDao {


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPosts(posts: List<Post>)

    @Query("SELECT * FROM post")
    fun getPost():Flow<List<Post>>

    @Query("DELETE FROM post")
    suspend fun deleteAll()

}