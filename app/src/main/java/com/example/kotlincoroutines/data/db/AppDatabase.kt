package com.example.kotlincoroutines.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.kotlincoroutines.data.model.Post

@Database(entities = [Post::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase(){

    abstract fun getPostDao(): PostDao


}